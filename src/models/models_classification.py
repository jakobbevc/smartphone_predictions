import pickle
import warnings
import sys

import pandas as pd
import numpy as np
import lightgbm as lgb

from time import time

from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.metrics import mean_squared_error, make_scorer
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, KFold
from sklearn.svm import SVC
from sklearn.metrics import plot_confusion_matrix
from sklearn.metrics import accuracy_score

from scipy.optimize import least_squares

# Adds higher directory to python modules path.
sys.path.append("..")
warnings.filterwarnings("ignore")

# LOCAL FILES
from utils.utils import classification_accuracy
from data.process_data import train_test_sets


def cross_valid(X_train, y_train, X_test, y_test, model_name):

    X_cross = np.concatenate([X_train, X_test], axis=0)
    y_cross = np.concatenate([y_train, y_test], axis=0)

    results_cross = []
    k_folds = KFold(n_splits=5, shuffle=False)
    for train, test in k_folds.split(X_cross):
        train_XX, test_XX = X_cross[train], X_cross[test]
        train_yy, test_yy = y_cross[train], y_cross[test]

        y_train_predict, y_test_predict = model_name(train_XX, train_yy, 
                                                     test_XX, load_model=False)

        accuracy = accuracy_score(test_yy, y_test_predict) * 100
        results_cross.append(accuracy)

    mean_acc = np.round(np.mean(results_cross), 2)
    std_dev = np.round(np.std(results_cross), 2)

    print("Cross validaton results:")
    print("Mean accuracy: " + str(mean_acc) + " +/- " + str(std_dev))


def GBM(X_train, y_train, X_test, load_model=False):
    if load_model:
        open_file = open("GMB_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = GradientBoostingRegressor(n_estimators=1000, learning_rate=0.05, max_depth=5, random_state=0, loss='ls')
        model.fit(X_train, y_train)
        save = open("GBM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def LGBM(X_train, y_train, X_test, load_model=False):

    train_dataset = lgb.Dataset(X_train, label=y_train)

    param = {}
    param['metric'] = ['multi_logloss']
    param['objective'] = 'multiclass'
    param['num_class'] = 4
    param['verbose'] = -1
    # params['importance_type'] = 'gain'
    # params['learning_rate'] = 0.05
    # params['boosting_type'] = 'gbdt'
    # params['objective'] = 'regression'
    # params['metric'] = 'mse'
    # params['num_leaves'] = 32
    # params['min_data_in_leaf'] = 30
    # params['max_depth'] = 5

    if load_model:
        open_file = open("LGBM_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = lgb.train(param, train_dataset, 100)
        save = open("..\\..\\models\\LGBM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return np.argmax(y_train_predict, axis=1), np.argmax(y_test_predict, axis=1)


def SVM(X_train, y_train, X_test, load_model=False):

    if load_model:
        open_file = open("SVM_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = SVC(C=1e4,
                    kernel='rbf',
                    cache_size=1000)

        model.fit(X_train, y_train)
        save = open("..\\..\\models\\SVM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def DNN(X_train, y_train, X_test, load_model=False):
    if load_model:
        open_file = open("DNN_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = MLPClassifier(hidden_layer_sizes=(60, 40, 30,),
                              activation='relu',
                              solver='adam',
                              alpha=1e-5,
                              batch_size='auto',
                              max_iter=int(1e5),
                              n_iter_no_change=50)

        model.fit(X_train, y_train)
        save = open("DNN_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def load_data(processed_data_path, file_name):
    path = processed_data_path + file_name

    print("Loading data...")
    df_data = pd.read_csv(path).sample(frac=1)

    features = ['battery_power', 'bluetooth', 'clock_speed', 'dual_sim', 'front_camera',
                'four_g', 'internal_memory', 'mobile_depth', 'mobile_weight', 'n_cores',
                'primary_camera', 'px_height', 'px_width', 'ram', 'screen_height',
                'screen_width', 'talk_time', 'three_g', 'touch_screen', 'wifi']
               

    pred_col = 'price_range'

    X_train, y_train, X_test, y_test = train_test_sets(df_data,
                                                        features,
                                                        pred_col,
                                                        test_size=0.3,
                                                        shuffle=False,
                                                        normalize=False)

    y_train, y_test = np.ravel(y_train), np.ravel(y_test)

    print("X_train shape is: {}".format(X_train.shape))
    print("y_train shape is: {}".format(y_train.shape))
    print("X_test shape is: {}".format(X_test.shape))
    print("y_test shape is: {}".format(y_test.shape))

    n_samples, n_features = X_train.shape
    print("Data was sucessfuly loaded.\n")

    return X_train, y_train, X_test, y_test, df_data[1400:]


if __name__ == '__main__':
    # Run models sequentially

    classes_dict = {0: 'medium', 1: 'high', 
                    2: 'very high', 3:'lower'}

    classes_list = list(classes_dict.values())

    raw_data_path = '..\\..\\data\\raw\\'
    processed_data_path = '..\\..\\data\\processed\\'
    save_results_path = '..\\..\\reports\\'

    CROSS_VAL = False

    # load data
    file_name = 'smartphone_processed.csv'
    X_train, y_train, X_test, y_test, df_data = load_data(processed_data_path,
                                                 file_name)

    # loop over all models in list
    prediction_models = [LGBM, SVM, DNN]
    column_names = ['LGBM', 'SVM', 'DNN']

    for model, column_name in zip(prediction_models, column_names):
        start_time = time()
        y_train_predict, y_test_predict = model(X_train, y_train, X_test)
        end_time = time()

        print("Time taken for training and testing: {0:.4f} s".format(end_time - start_time))
        print("Accuracy {}:".format(column_name))

        classification_accuracy(y_train, y_train_predict, 
                                y_test, y_test_predict)

        if CROSS_VAL:
            cross_valid(X_train, y_train, X_test, y_test, model)

        print("########################################################\n")

        # # APPENDING NEW RESULTS TO EXISTING TABLE ##
        # y_true = np.hstack((y_train, y_test)).T
        # y_prediction_np = np.hstack((y_train_predict, y_test_predict)).T
    
        pred_column_name = 'predicted_label_' + column_name
        df_data[pred_column_name] = y_test_predict

    df_data.to_csv(processed_data_path +'smartphone_classification.csv')

