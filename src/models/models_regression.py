import pickle
import warnings
import sys

import pandas as pd
import numpy as np
import lightgbm as lgb

from time import time

from sklearn.linear_model import LinearRegression
from sklearn.neural_network import MLPRegressor
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.metrics import mean_squared_error, make_scorer
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV, KFold
from sklearn import svm
from sklearn.metrics import plot_confusion_matrix
from sklearn.metrics import accuracy_score, mean_squared_error

from scipy.optimize import least_squares

# Adds higher directory to python modules path.
sys.path.append("..")
warnings.filterwarnings("ignore")

# LOCAL FILES
from utils.utils import classification_accuracy, RMSE_ERROR
from data.process_data import train_test_sets


def cross_valid(X_cross, y_cross, model_name):
    results_cross = []
    k_folds = KFold(n_splits=5, shuffle=False)
    for train, test in k_folds.split(X_cross):
        train_XX, test_XX = X_cross[train], X_cross[test]
        train_yy, test_yy = y_cross[train], y_cross[test]

        y_train_predict, y_test_predict = model_name(train_XX, train_yy, 
                                                     test_XX, load_model=False)

        rmse_error = np.sqrt(mean_squared_error(test_yy, y_test_predict))
        results_cross.append(rmse_error)

    mean_acc = np.round(np.mean(results_cross), 2)
    std_dev = np.round(np.std(results_cross), 2)

    print("Cross validaton results:")
    print("Mean accuracy: " + str(mean_acc) + " +/- " + str(std_dev))


def GBM(X_train, y_train, X_test, load_model=False):
    if load_model:
        open_file = open("GMB_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = GradientBoostingRegressor(n_estimators=1000, learning_rate=0.05, max_depth=5, random_state=0, loss='ls')
        model.fit(X_train, y_train)
        save = open("GBM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def LGBM(X_train, y_train, X_test, load_model=False):

    train_dataset = lgb.Dataset(X_train, label=y_train)

    param = {}
    param['metric'] = 'rmse'
    param['objective'] = 'regression'
    param['verbose'] = -1
    # params['importance_type'] = 'gain'
    # params['num_iterations'] = 15000
    # params['num_iterations'] = 200
    # params['learning_rate'] = 0.05
    # params['learning_rate'] = 0.001
    # params['boosting_type'] = 'gbdt'
    # params['objective'] = 'regression'
    # params['metric'] = 'mse'
    # params['dart'] = True,
    # params['sub_feature'] = 0.5
    # params['num_leaves'] = 32 #32
    # params['min_data_in_leaf'] = 30
    # params['max_depth'] = 5
    # params['max_bin'] = 128
    # params['bagging_fraction'] = 0.5
    # params['bagging_freq'] = 32

    if load_model:
        open_file = open("LGBM_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = lgb.train(param, train_dataset, 100)
        # lgb.plot_importance(model, ax=None, height=0.2, xlim=None, ylim=None, title='Feature importance', xlabel='Feature importance',importance_type='gain', ylabel='Features', max_num_features=None, ignore_zero=True, figsize=None, grid=True, precision=None)
        # plt.show()
        save = open("..\\..\\models\\LGBM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def SVM(X_train, y_train, X_test, load_model=False):
    if load_model:
        open_file = open("SVM_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = svm.SVR(C=1e4,
                        kernel='rbf',
                        cache_size=1000)  

        model.fit(X_train, y_train)
        save = open("..\\..\\models\\SVM_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def DNN(X_train, y_train, X_test, load_model=False):
    if load_model:
        open_file = open("DNN_parameters.pickle", "rb")
        model = pickle.load(open_file)
    else:
        model = MLPRegressor(hidden_layer_sizes=(60,40,30,),
                             activation='relu',
                             solver='adam',
                             alpha=1e-5,
                             batch_size='auto',
                             learning_rate='constant',
                             max_iter=int(1e5),
                             n_iter_no_change=50)

        model.fit(X_train, y_train)
        save = open("DNN_parameters.pickle", "wb")
        pickle.dump(model, save)

    y_train_predict = model.predict(X_train)
    y_test_predict = model.predict(X_test)

    return y_train_predict, y_test_predict


def RandomSearch(model, param_dist, n_iter_search, X_train, y_train):

    random_search = RandomizedSearchCV(model, param_distributions = param_dist,
                                       n_iter = n_iter_search, cv = 5, iid = False)
    start = time()

    random_search.fit(X_train, y_train)

    print("RandomizedSearchCV took %.2f seconds for %d candidates"
          " parameter settings." % ((time() - start), n_iter_search))
    report(random_search.cv_results_)


if __name__ == '__main__':

    #############################################################################
    ## Run all models at the same time
    #############################################################################
    for i in range(1):
        classes_dict = {0: 'medium', 1: 'high', 
                        2: 'very high', 3:'lower'}

        classes_list = list(classes_dict.values())

        raw_data_path = '..\\..\\data\\raw\\'
        processed_data_path = '..\\..\\data\\processed\\'

        save_results_path = '..\\..\\reports\\'

        CROSS_VAL = False

        path = processed_data_path + 'smartphone_processed.csv'

        print("Loading data...")
        df_data = pd.read_csv(path)

        features = ['battery_power', 'bluetooth', 'clock_speed', 'dual_sim', 'front_camera',
                    'four_g', 'internal_memory', 'mobile_depth', 'mobile_weight', 'n_cores',
                    'primary_camera', 'px_height', 'px_width', 'ram', 'screen_height',
                    'screen_width', 'talk_time', 'three_g', 'touch_screen', 'wifi',
                    'price_range', 'price']

        pred_col = 'price'
        print(features)
        X_train, y_train, X_test, y_test = train_test_sets(df_data,
                                                           features,
                                                           pred_col,
                                                           test_size=0.3,
                                                           shuffle=True,
                                                           normalize=False)

        y_train, y_test = np.ravel(y_train), np.ravel(y_test)

        print("X_train shape is: {}".format(X_train.shape))
        print("y_train shape is: {}".format(y_train.shape))
        print("X_test shape is: {}".format(X_test.shape))
        print("y_test shape is: {}".format(y_test.shape))

        n_samples, n_features = X_train.shape
        print("Data of size: ({}, {}) was sucessfuly loaded".format(n_samples, n_features))

        # Loop over all models in list
        prediction_models = [LGBM, SVM, DNN]
        column_names = ['LGBM', 'SVM', 'DNN']
        # prediction_models = [SVM, DNN]
        # column_names = ['SVM', 'DNN']
        # prediction_models = [DNN]
        # column_names = ['DNN']
        # prediction_models = [LGBM, SVM]
        # column_names = ['LGBM', 'SVM']

        for model, column_name in zip(prediction_models, column_names):

            start_time = time()
            y_train_predict, y_test_predict = model(X_train, y_train, X_test, load_model=False)
            end_time = time()

            print("Time taken for training and testing: {0:.4f} s".format(end_time - start_time))
            print("Accuracy {}:".format(column_name))
            print(y_train_predict.shape)
            print(y_test_predict.shape)
            RMSE_ERROR(y_train, y_train_predict, y_test, y_test_predict)
            print("########################################################\n")

            if CROSS_VAL:
                X_cross = np.concatenate([X_train, X_test], axis=0)
                y_cross = np.concatenate([y_train, y_test], axis=0)
                cross_valid(X_cross, y_cross, model)


 
################
# RANDOM SEARCH FOR HYPERPARAMETERS
################

    # param_dist = {'kernel' : ['sigmoid'],
    #               'C' : [0.001, 0.01, 0.1, 0.5],
    #               'gamma' : [1e-4, 1e-3],
    #               'epsilon' : [0.01, 0.1, 1]}
    #
    # n_iter_search = 20
    # RandomSearch(svm.SVR(), param_dist, n_iter_search, X_train, y_train)


################
# GRID SEARCH FOR HYPERPARAMETERS
################

#     tuned_parameters = [{
#                         'kernel' : ['linear', 'rbf', 'sigmoid'],
#                         'C' : [0.1, 1],
#                         'gamma' : [0.1],
#                         #'epsilon' : [1e-2, 1e-3, 1e-4]
#                         }]
#
#
#     # tuned_parameters = [{'kernel' : ['linear'], 'C' : [0.0001, 0.001], 'gamma' : [1e-3], 'epsilon' : [1e-2, 1e-3]}]
#     mean_squared_error_scorer = make_scorer(mean_squared_error)
#     scores = [mean_squared_error_scorer]
#
#     for score in scores:
#         print("# Tuning hyper-parameters for {}".format(score))
#         print()
#
#         reg = GridSearchCV(svm.SVR(), tuned_parameters, cv = 5,
#                             scoring = score) #cv number of fold in cross validation
#         reg.fit(X_train, y_train)
#
#         print("Best parameters set found on development set:")
#         print()
#         print(reg.best_params_)
#         print()
#         print("Grid scores on development set:")
#         print()
#         means = reg.cv_results_['mean_test_score']
#         stds = reg.cv_results_['std_test_score']
#         for mean, std, params in zip(means, stds, reg.cv_results_['params']):
#             print("%0.3f (+/-%0.03f) for %r" % (mean, std * 2, params))
#         print()
#
#         print("Detailed classification report:")
#         print()
#         print("The model is trained on the full development set.")
#         print("The scores are computed on the full evaluation set.")
#         print()
#         #y_true, y_pred = y_test, reg.predict(X_test)
#         #print(classification_report(y_true, y_pred))
#         #print()
