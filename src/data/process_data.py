import sys
import os

import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
from sklearn import preprocessing

from scipy import signal

sys.path.append("..")


def low_pass_filter_parameters(df_data, data_len_seconds, f_cut_off=0.25):
    if data_len_seconds is None:
        min_time = min(df_data.index)
        max_time = max(df_data.index)

        data_len_seconds = (max_time - min_time).total_seconds()
    else:
        data_len_seconds = data_len_seconds

    num_samples = df_data.shape[0]

    fs = num_samples / data_len_seconds
    fc = f_cut_off  # Cut-off frequency of the filter

    w = fc / (fs / 2) # Normalize the frequency
    b, a = signal.butter(5, w, 'low')
    
    return b, a


def extract_AC_DC_components(df_data, columns_to_extract, data_len_seconds):
    
    b, a = low_pass_filter_parameters(df_data, data_len_seconds, f_cut_off=0.25)
    
    for column_name in columns_to_extract:
    
        current_signal = df_data[column_name]

        current_signal_DC = signal.filtfilt(b, a, current_signal)
        current_signal_AC = current_signal - current_signal_DC

        column_name_DC = column_name + '_DC'
        column_name_AC = column_name + '_AC'

        df_data[column_name_DC] = current_signal_DC
        df_data[column_name_AC] = current_signal_AC


def rename_columns(df_data):
    old_names = list(df_data.columns)
    
    # strip spaces around column names
    new_names = [name.strip() for name in old_names]

    name_mapper = dict(zip(old_names, new_names))
    
    df_data = df_data.rename(columns=name_mapper)
    
    return df_data



def normalize_data(X_train, X_test):
    std_scale = preprocessing.StandardScaler().fit(X_train)
    X_train_norm = std_scale.transform(X_train)
    X_test_norm = std_scale.transform(X_test)

    return X_train_norm, X_test_norm


def train_test_sets(df_data, features, predicted_column, test_size=0.3, shuffle=False, normalize=False):

    X = df_data[features].values
    y = df_data[predicted_column].values

    X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                        test_size=test_size,
                                                        shuffle=shuffle)
    y_train, y_test = np.ravel(y_train), np.ravel(y_test)

    if normalize:
        X_train, X_test = normalize_data(X_train, X_test)

    return X_train, y_train, X_test, y_test


if __name__ == '__main__':

    raw_data_path = '..\\..\\data\\raw\\'
    processed_data_path = '..\\..\\data\\processed\\'

    file_list = []
    for file in os.listdir(raw_data_path):
        if file.endswith('.xlsx'):
            file_list.append(file)
            print(file)

    df_concated = pd.DataFrame()
    file_len = [None, None, 433, 318, 334, None, None]

    for file_name, file_len_sec in zip(file_list, file_len):
        df_data = pd.DataFrame()
        path = raw_data_path + file_name

        df_data = pd.read_excel(path)
        if file_len_sec is None:
            df_data.set_index(pd.DatetimeIndex(df_data['timestamp']),
                                inplace=True)

        df_data.dropna(axis=0, inplace=True)
        df_data = rename_columns(df_data)

        columns_to_ext = ['acc_x', 'acc_y', 'acc_z']
        extract_AC_DC_components(df_data, columns_to_ext, file_len_sec)
     
        temp_df = pd.concat((df_concated, df_data))
        df_concated = temp_df

    scaler = MinMaxScaler(feature_range=(-1, 1))
    col_to_remove = ['timestamp', 'label', 'sensor']
    all_col = list(df_concated.columns)

    reduce_columns = [col for col in all_col if col not in col_to_remove]

    data = df_concated[reduce_columns]
    labels = df_concated['label'] - 1

    data[reduce_columns] = scaler.fit_transform(data)
    data['labels'] = labels

    # data.to_csv(processed_data_path + "all_datasets_combined.csv", 
    #             index=False)